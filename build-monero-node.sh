#!/bin/bash

MONERO_VERSION=v0.18.3.1


# ==================================================================================================
function show_splash {
    # credit: https://monerobr0.github.io/ASCII-ART/
    cat << EOF
                                        │                                               
                                        │                    ┏────                      
                                       ┏┛                 ┏──┛                          
               │                       ┗┳─┓            ┏──┫                             
               │          │         ┏───┛ ┗─────┓     ┏┛ ┏┛                             
               ┗──┓       ┗─┓  ┏────┛▓▓▓▓▓▓▓▓▓▓▓┗────┳┛  │                              
                  │         ┣──┛▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┗──┳┛                              
                  │       ┏─┛▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┗─┓   ┏─                        
                  ┗──┓  ┏─┛▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┗─┳─┛     │                   
        ──┓          ┗┳─┛▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┗─┓     ┣─────              
          ┗────┓     ┏┛▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┗┓┏───┛                   
               ┗──┓ ┏┛▓▓▓▓▓│▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓│▓▓▓▓▓┗┫                       
                  ┗┳┛▓▓▓▓▓▓┣──┓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┏──┫▓▓▓▓▓▓┗┓                      
                  ┏┛▓▓▓▓▓▓▓│██┗─┓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┏─┛██│▓▓▓▓▓▓▓┗┓                     
                  │▓▓▓▓▓▓▓▓│████┗─┓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┏─┛████│▓▓▓▓▓▓▓▓│                     
                 ┏┛▓▓▓▓▓▓▓▓│██████┗─┓▓▓▓▓▓▓▓▓▓▓▓┏─┛██████│▓▓▓▓▓▓▓▓┗┓                    
                 │▓▓▓▓▓▓▓▓▓│████████┗─┓▓▓▓▓▓▓▓┏─┛████████│▓▓▓▓▓▓▓▓▓│   ┏┓               
       ─┓     ┏──┫▓▓▓▓▓▓▓▓▓│███┏──┓███┗─┓▓▓▓┏─┛███┏──┓███│▓▓▓▓▓▓▓▓▓┣───┛┗────           
        ┗──┓  │  ┗┓▓▓▓▓▓▓▓▓│███│  ┗─┓███┗───┛███┏─┛  │███│▓▓▓▓▓▓▓▓┏┛                    
           ┗──┛   │▓▓▓▓▓▓▓▓│███│    ┗─┓███████┏─┛    │███│▓▓▓▓▓▓▓▓│                     
                  ┗────────┛███│      ┗─┓███┏─┛      │███┗────────┛                     
  ░░░░▒▒▒▒▒▓▓▓▓▓▓██████████████│        ┗─┳─┛        │███████████████▓▓▓▓░░░░░░         
   ░░▒▒▒▒▓▓▓▓▓▓████████████████│                     │████████████████▓▓▓▓▓▓▓▓░░░       
                      ┏────────┛                     ┗────────┓                         
                      │                                       │                         
                      ┗─┓                                   ┏─┛                         
                       ┏┻─┓                               ┏─┻─┓ ┏──                     
                     ──┛  ┗─┓                           ┏─┫   ┗─┛                       
                         ┏┓ ┣───┓                   ┏───┛ ┗─┓                           
                       ┏─┛┗─┛   ┗┳───┓         ┏────┻┓      ┗─                          
                      ┏┛        ─┛   ┗┳───────┳┛     ┗┓                                 
                    ──┛              ┏┛       ┗┓      ┗┓                                
                                     │         ┗┓      ┗──                              
                                                │                                       
EOF
}


# --------------------------------------------------------------------------------------------------
function show_help {
    show_splash
    cat << EOF
Help goes here
EOF
}


# --------------------------------------------------------------------------------------------------
function print_status {
    echo -e "\n\033[0;35m$1\033[0m"
}


# --------------------------------------------------------------------------------------------------
function check_flag {
    flag=$1; shift
    while test $# -gt 0; do
        if [ "$flag" = "$1" ]; then
            return 0
        fi
        shift
    done
    return 1
}


# --------------------------------------------------------------------------------------------------
function pause {
    read -n 1 -s -r -p "Press any key to continue..."
    echo -e "\n"
}


# --------------------------------------------------------------------------------------------------
function do_build {
    ## https://github.com/monero-project/monero?tab=readme-ov-file#on-the-raspberry-pi
    T_START=$(date +%s)

    # Do dependency check and install
    if ! check_flag "--skip-depends" "$@"; then
        print_status "Performing apt update/upgrade and installing required dependencies"
        sudo apt update
        sudo apt upgrade -y
        sudo apt install -y libc-devtools javascript-common -y
    else
        print_status "WARN: User requested to skip apt update/upgrade and installing required dependencies"
    fi

    # Clone git repo(s)
    print_status "Cloning repo and and checking out $MONERO_VERSION"
    mkdir -p "$HOME/monero-data/.builds"
    cd "$HOME/monero-data/.builds"
    git clone --recursive https://github.com/monero-project/monero.git "$MONERO_VERSION"

    cd "$MONERO_VERSION"
    git checkout "$MONERO_VERSION"
    git submodule update --init --force

    # Build from source
    print_status "Building binaries from source. (This may take quite a while.)"
    USE_SINGLE_BUILDDIR=1 make release

    # Create backup archive of the binaries
    if check_flag "--skip-archive" "$@"; then
        print_status "INFO: User requested to skip archiving of binaries."
    else
        print_status "Archiving binaries to '~/monero-data/.builds/$MONERO_VERSION.tar.bz2'."
        tar -cvjSf "../$MONERO_VERSION.tar.bz2" "build/release/bin/*"
    fi

    # Add most recent binaries to $PATH
        print_status "Adding binaries to PATH variable."
    cat << EOF > ~/monero-data/.monero-path
export PATH="$PATH:$HOME/monero/.builds/$MONERO_VERSION/build/release/bin"
EOF
    if ! grep -q "$HOME/monero-data/.monero-path" < "$HOME/.profile"; then
        echo -e "\n# Monero\nsource $HOME/monero-data/.monero-path" >> "$HOME/.profile"
    fi
    source "$HOME/.profile"
    
    # All done
    print_status "All done!"
    T_END=$(date +%s)
    echo " - Total script execution time: $(( T_END - T_START ))"
    echo " - A reboot is advised."
    echo " - You can start the monero daemon by running 'monerod -detach'"
    echo ""
    
    pause
}


# ==================================================================================================
if check_flag "--help" "$@"; then
    show_help
elif check_flag "byobu-session" "$@"; then
    show_splash
    do_build "@"
    sleep 20
else
    if dpkg-query -s byobu | grep -q Status; then
        echo "Byobu already installed"
    else
        echo "Install byobu"
        sudo apt install -y byobu
    fi
    byobu new-session -s "build@monero" "$0 byobu-session $*"
    # byobu attach-session -t "build@monero"
fi

exit 0
