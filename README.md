# pi-zero-monero
## intro
Bunch of scripts for a Pi Zero running [DietPi](https://dietpi.com/). Tested on a [Pi Zero 1.3](https://www.raspberrypi.com/products/raspberry-pi-zero/).

## scripts
### build-monero-node.sh
Build a full monero node autonymously.

### fix-curl-timeout.sh
Patch to fix a curl timeout error caused by super slow ping.

### fix-dbus-error.sh
Patch to fix the dbus error message that occurs by using some SD cards.

### fancy_bash.sh
Makes the prompt nicer and performs a few tweaks for a better console experience.
