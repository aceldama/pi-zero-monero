#!/bin/bash


cat << EOF >> "$HOME/.bashrc"

# Make bash fancy :)
source "$HOME/.bash_fancy"
EOF

ln -s "$(dirname "$(realpath -s "$0")")" "$HOME/.bash_fancy"
source "$HOME/.bashrc"
