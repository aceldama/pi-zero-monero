#!/bin/bash

## credit: AceldamA -> https://github.com/MichaIng/DietPi/issues/2717#issuecomment-1492680455
cat << EOF | sudo tee -a /etc/resolv.conf
# Curl timeout patch (https://serverfault.com/a/858665)
options single-request-reopen
EOF
